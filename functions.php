<?php
/**
 * Primary functionality for the Stephanie Ryall WordPress theme
 *
 * @package    WordPress
 * @subpackage Stephanie Ryall
 * @version    1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have permission to access this file directly.' );
}

if ( ! class_exists( 'Stephanie_Ryall' ) ) {
	class Stephanie_Ryall {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $version = '1.0';
		
		/**
		 * Holds the class instance.
		 *
		 * @since     0.1
		 * @access    private
		 * @var        Stephanie_Ryall
		 */
		private static $instance;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   0.1
		 * @return    Stephanie_Ryall
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className      = __CLASS__;
				self::$instance = new $className;
			}
			
			return self::$instance;
		}
		
		/**
		 * Create the object and set up the appropriate actions
		 *
		 * @access  private
		 * @since   0.1
		 */
		private function __construct() {
			//* Child theme (do not remove)
			define( 'CHILD_THEME_NAME', 'Stephanie Ryall 2017' );
			define( 'CHILD_THEME_URL', 'http://www.stephanieryall.com/' );
			define( 'CHILD_THEME_VERSION', $this->version );
			
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			add_action( 'init', array( $this, 'init' ) );
		}
		
		/**
		 * Register/enqueue any required style sheets and script files
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function add_scripts_and_styles() {
			wp_register_style( 'genesis', get_stylesheet_uri(), array( 'dashicons' ), $this->version, 'all' );
			wp_enqueue_style( 'ryall', get_stylesheet_directory_uri() . '/stephanie-ryall.css', array( 'genesis' ), $this->version, 'all' );
		}
		
		/**
		 * Perform any actions that need to occur before the page is output
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function template_redirect() {
			if ( $this->is_blog_page() ) {
				add_filter( 'excerpt_more', array( $this, 'read_more_link' ), 10, 0 );
				add_filter( 'get_the_content_more_link', array( $this, 'read_more_link' ), 10, 0 );
				add_filter( 'the_content_more_link', array( $this, 'read_more_link' ), 10, 0 );
				add_filter( 'the_excerpt', array( $this, 'read_more_link' ), 10, 1 );
				add_filter( 'the_content', array( $this, 'read_more_link' ), 10, 1 );
			}
			
			if ( is_front_page() ) {
				remove_all_actions( 'genesis_loop' );
				remove_all_actions( 'genesis_before_loop' );
				remove_all_actions( 'genesis_after_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page' ) );
			}
			if ( ( is_singular() && ! is_singular( 'post' ) ) || is_post_type_archive( 'testimonial' ) ) {
				remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
			}
			
			if ( is_post_type_archive( 'testimonial' ) ) {
				remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );
				add_filter( 'get_the_content_limit', function() { return get_the_content(); }, 10, 2 );
			}
			
			/*if ( is_home() || is_post_type_archive( 'post' ) || is_category() || is_tag() ) {
				remove_filter( 'the_posts', array( 'dsIdxListingsPages', 'DisplayPage' ), 100 );
				remove_filter( "the_posts", array( "dsSearchAgent_Client", "Activate" ) );
			}*/
		}
		
		/**
		 * Attempt to stop dsIDXPress from screwing up the genesis_do_post_content() function
		 *
		 * @access public
		 * @since  0.1
		 * @return void
		 */
		public function genesis_do_post_content() {
			if ( is_singular() ) {
				global $wp_query;
				$wp_query->is_singular = false;
			}
			
			genesis_do_post_content();
		}
		
		/**
		 * Perform any necessary modifications to the Genesis defaults
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function adjust_genesis() {
			remove_theme_support( 'genesis-inpost-layouts' );
			foreach ( array( 'content-sidebar', 'sidebar-content', 'content-sidebar-sidebar', 'sidebar-sidebar-content', 'sidebar-content-sidebar' ) as $layout ) {
				genesis_unregister_layout( $layout );
			}
			
			add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-accessibility', array(
				'404-page',
				'drop-down-menu',
				'headings',
				'rems',
				'search-form',
				'skip-links'
			) );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'menu-primary',
				'menu-secondary',
				/*'site-inner',*/
				'footer-widgets',
				'footer',
				'front-sidebar',
			) );
			
			/** Add support for custom header **/
			$header_args = array(
				'default-image'          => get_stylesheet_directory_uri() . '/images/header.png',
				'width'                  => 1600,
				'height'                 => 245,
				'flex-width'             => false,
				'flex-height'            => true,
				'uploads'                => true,
				'random-default'         => false,
				'header-text'            => false,
				'default-text-color'     => '',
				'wp-head-callback'       => array( $this, 'do_header_style' ),
				'admin-head-callback'    => '',
				'admin-preview-callback' => '',
				'video'                  => false,
				'video-active-callback'  => '',
			);
			add_theme_support( 'custom-header', $header_args );
			
			/** Add support for a custom logo */
			add_theme_support( 'custom-logo', array(
				'default-image' => get_stylesheet_directory_uri() . '/images/logo.png',
				'width'         => 873,
				'height'        => 254,
				'flex-height'   => true,
			) );
			
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			remove_all_actions( 'genesis_header' );
			add_action( 'genesis_header', array( $this, 'do_header' ) );
			remove_action( 'genesis_footer', 'genesis_do_footer' );
			add_action( 'genesis_footer', array( $this, 'do_footer' ) );
			
			add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
			remove_all_actions( 'genesis_sidebar' );
			remove_all_actions( 'genesis_sidebar_alt' );
			
			remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
			add_filter( 'genesis_post_info', array( $this, 'genesis_post_info' ) );
			
			global $content_width;
			$content_width = 1280;
			
			remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
			remove_action( 'genesis_post_content', 'genesis_do_post_image' );
			add_action( 'genesis_entry_header', 'genesis_do_post_image', 8 );
			add_action( 'genesis_before_post_title', 'genesis_do_post_image' );
			
			add_post_type_support( 'team-member', 'genesis-cpt-archives-settings' );
			add_post_type_support( 'testimonial', 'genesis-cpt-archives-settings' );
		}
		
		/**
		 * Determine whether this is a posts archive page or not
		 *
		 * @access public
		 * @since  0.1
		 * @return bool whether this is a posts archive page
		 */
		public function is_blog_page() {
			$rt = false;
			if ( is_home() ) {
				$rt = true;
			}
			if ( is_tag() ) {
				$rt = true;
			}
			if ( is_category() ) {
				$rt = true;
			}
			if ( is_post_type_archive( 'post' ) ) {
				$rt = true;
			}
			
			return $rt;
		}
		
		/**
		 * Register any necessary widgetized areas
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function register_sidebars() {
			/**
			 * Register the front page widget areas
			 */
			genesis_register_sidebar( array(
				'id'          => 'front-feature',
				'name'        => __( '[Front Page] Main Feature', 'stephanie-ryall' ),
				'description' => __( 'Appears full-width at the top of the home page', 'stephanie-ryall' ),
			) );
			
			genesis_register_sidebar( array(
				'id'          => 'front-secondary',
				'name'        => __( '[Front Page] Secondary Features', 'stephanie-ryall' ),
				'description' => __( 'Appears below the main feature; items are divided into thirds across the full-width page', 'stephanie-ryall' ),
			) );
			
			genesis_register_sidebar( array(
				'id'          => 'front-footer',
				'name'        => __( '[Front Page] Above Footer', 'stephanie-ryall' ),
				'description' => __( 'Appears just above the footer on the home page; items are divided into halves across the full-width page', 'stephanie-ryall' ),
			) );
			
			/**
			 * Register the footer widget areas
			 */
			genesis_register_sidebar( array(
				'id'          => 'footer-1',
				'name'        => __( '[Footer] Area 1', 'stephanie-ryall' ),
				'description' => __( 'Displays as the first widget area in the footer', 'stephanie-ryall' ),
			) );
			
			genesis_register_sidebar( array(
				'id'          => 'footer-2',
				'name'        => __( '[Footer] Area 2', 'stephanie-ryall' ),
				'description' => __( 'Displays as the second widget area in the footer', 'stephanie-ryall' ),
			) );
			
			genesis_register_sidebar( array(
				'id'          => 'footer-3',
				'name'        => __( '[Footer] Area 3', 'stephanie-ryall' ),
				'description' => __( 'Displays as the third widget area in the footer', 'stephanie-ryall' ),
			) );
		}
		
		/**
		 * Perform any actions that need to occur at the start of the bootstrap
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function init() {
			add_image_size( 'main-feature', 1600, 600, true );
			add_image_size( 'team-member', 250, 300, true );
		}
		
		/*
		 * Output the page header
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_header() {
			echo '<header class="site-header"><div class="wrap">';
			if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
			}
			echo '</div></header>';
		}
		
		/**
		 * Output the CSS declaration for the custom header image
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_header_style() {
			printf( '<style type="text/css">.site-header > .wrap { background-image: url(%s) }</style>', get_header_image() );
		}
		
		/**
		 * Output the page footer
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_footer() {
			echo '<footer class="site-footer">';
			genesis_structural_wrap( 'footer' );
			
			echo '<section class="footer-widgets-1 footer-widgets">';
			dynamic_sidebar( 'footer-1' );
			echo '</section>';
			
			echo '<section class="footer-widgets-2 footer-widgets">';
			dynamic_sidebar( 'footer-2' );
			echo '</section>';
			
			echo '<section class="footer-widgets-3 footer-widgets">';
			dynamic_sidebar( 'footer-3' );
			$this->white_spider_notice();
			echo '</section>';
			
			genesis_structural_wrap( 'footer', 'close' );
			echo '</footer>';
		}
		
		/**
		 * Output the main content of the front page
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_front_page() {
			if ( is_active_sidebar( 'front-feature' ) ) {
				echo '<section class="home-feature">';
				genesis_structural_wrap( 'front-sidebar' );
				dynamic_sidebar( 'front-feature' );
				genesis_structural_wrap( 'front-sidebar', 'close' );
				echo '</section>';
			}
			
			if ( is_active_sidebar( 'front-secondary' ) ) {
				echo '<section class="home-secondary">';
				genesis_structural_wrap( 'front-sidebar' );
				dynamic_sidebar( 'front-secondary' );
				genesis_structural_wrap( 'front-sidebar', 'close' );
				echo '</section>';
			}
			
			if ( is_active_sidebar( 'front-footer' ) ) {
				echo '<section class="home-footer">';
				genesis_structural_wrap( 'front-sidebar' );
				dynamic_sidebar( 'front-footer' );
				genesis_structural_wrap( 'front-sidebar', 'close' );
				echo '</section>';
			}
			
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				echo '<section class="home-content">';
				genesis_structural_wrap( 'front-sidebar' );
				if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'thumbnail', array( 'class' => 'alignright home-content-feature' ) );
				}
				printf( '<h1 class="entry-title home-content-title">%s</h1>', apply_filters( 'the_title', get_the_title() ) );
				printf( '<div class="entry-content home-content-entry">%s</div>', apply_filters( 'the_content', get_the_content() ) );
				genesis_structural_wrap( 'front-sidebar', 'close' );
				echo '</section>';
			endwhile; endif;
		}
		
		/**
		 * Modify the "read more" ellipsis/link for excerpts/limited content
		 *
		 * @param $content string the content being filtered
		 *
		 * @access public
		 * @since  0.1
		 * @return string the read more link HTML
		 */
		public function read_more_link( $content='' ) {
			if ( 'post' != get_post_type() ) {
				return $content;
			}
			
			if ( empty( $content ) ) {
				return '&hellip;';
			}
			
			return $content . '<p><a class="more-link" href="' . get_permalink() . '">Read more</a></p>';
		}
		
		/**
		 * Modify the entry header for blog posts
		 *
		 * @param   $info string the existing shortcode string being modified
		 *
		 * @access  public
		 * @since   0.1
		 * @return  string the updated shortcode string
		 */
		public function genesis_post_info() {
			return '[post_date] [post_comments] [post_edit]';
		}
		
		/**
		 * Output the White Spider notice
		 *
		 * @access  private
		 * @since   0.1
		 * @return  void
		 */
		private function white_spider_notice() {
			$notice = sprintf( __( '<p>Site by <a href="%2$s">%3$s</a></p>', 'stephanie-ryall' ), get_bloginfo( 'name' ), 'http://whitespiderinc.com/', 'White Spider' );
			printf( '<div id="white-spider-copyright-widget-1" class="widget white-spider-copyright"><div class="widget-wrap"><div class="textwidget">%s</div></div></div>', $notice );
		}
	}
}

global $ryall_object;
$ryall_object = Stephanie_Ryall::instance();